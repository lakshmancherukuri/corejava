package oops;
//string manipulation
public class Stringmanuplation {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		String s="laxman";
		System.out.println(s);
		String s1=new String("ram");
		System.out.println(s1);
		char []ch= {'a','b','c','d','e','f'};
		String s2=new String(ch,2,2);
		System.out.println(s2);
		String s3="srinu";
		String s4="srinu";
		System.out.println(s3==s4);//== always used reference comparison
		String s5=new String("ramm");
		s5=s5.concat("urthy");
		System.out.println(s5);
		StringBuffer s6 =new StringBuffer("cherukuri");
		s6.append("lakshman");
		System.out.println(s6);
		
		
		
		
	}
	}
class equalsmethod{
	//equals method belongs to object class and used to reference comparison
	//string overriding equals method() for content comparison
	//string class is child class of object class
	//stringBuffer class their no equals  and not overriding equals() 
	//And stringBuffer uses parent class(object class) equals for reference comparison
	//== always used reference comparison
	equalsmethod(String str){
		
	}
	public static void main(String[] args) {
		equalsmethod em=new equalsmethod("laxman");
		equalsmethod em1=new equalsmethod("laxman");
		System.out.println(em.equals(em1));
		System.out.println(em==em1);
		
		String str=new String("laxman");
		String str1=new String("laxman");
		System.out.println(str.equals(str1));
		System.out.println(str==str1);
		
		StringBuffer sb1=new StringBuffer("cherukuri");
		StringBuffer sb2=new StringBuffer("cherukuri");
		System.out.println(sb1.equals(sb2));
		System.out.println(sb1==sb2);
		
		
		
		
		
	}

}
class tostring{
	String s;
public	tostring(String s){
		this.s=s;
	}
	
	@Override
public String toString() {
	return "tostring []"+s;
}

	public static void main(String []args) {
		tostring ts=new tostring("laxman");
		System.out.println(ts);
		System.out.println(ts.toString());
		
		String sr=new String("laxman");
		System.out.println(sr.toString());
		StringBuffer sbr=new StringBuffer("cherukuri");
		System.out.println(sbr);
	}
}
//String class method
class stringclassmethod{
	public static void main(String []args) {
	
	String s ="   welcome to laxman cherukuri   ";
	System.out.println(s.length());
	System.out.println(s.charAt(26));
	System.out.println(s.contains("welcome"));
	System.out.println(s);
	System.out.println(s.length());
	System.out.println(s.trim().length());
	System.out.println(s.trim().substring(3).length());
	String []s1=s.split("");
	for(String ss:s1) {
		System.out.println(ss);

	}
	
	
}
	}