package oops;

// normal class and its class name i return as abstract for m understanding//jenkins integration//
public  class Abstract1 {
void m1(int a,String s) {
	System.out.println("First method");
	System.out.println(a+s);
}
	void m2() {
		System.out.println("second method");
	}
public static void main(String []args) {
	
	Abstract1 t1=new Abstract1();
	t1.m1(10,"laxman");
	

	t1.m2();
	
	
}

}

// abstract class may contains abstract methods or may not contain abstract methods
abstract class Test2{
	abstract void m1() ;
	abstract void m3();
		void m2() {
	 System.out.println("method m2");
 }

}
// complete implementation only we can create the object in child class.
//other wise we can call childclass also a abstract class,
//by using abstract keyword infront of child class
 abstract class Test3 extends Test2{
	void m1() {
		
		System.out.println("m1 method");
	}
	
}
  class Test4 extends Test3{
	 void m3() {
			System.out.println("m3 method");

	 }
	 public static void main(String []args) { 
		 Test2 t2=new Test4(); 
		 t2.m1();t2.m2();t2.m3();
		 
	 }	 
 }
 // inside the abstract class constructor is possible
  //note without child class we cannot execute constructor, by using super keyword for constructor execution
 abstract class constructor{
	 constructor(){
		 System.out.println("abstract class constructor is executed");
	 }	 
 }
	class constructor1 extends constructor{
		constructor1(){
			 System.out.println("constructor is executed");

		}
	 
	 public static void main(String []args) { 
		 
		 new constructor1();
	 }
 }
 
 // inside abstract class main method is possible
	abstract class mainmethod{
		public static void main(String []args) {
			System.out.println("main method wworks in abstract class successfully");
		}
	}
// abstract class with return type and without void
	
	abstract class without_void{
		abstract int m1(int a);
		abstract String m2(int a, String s);
		abstract boolean m3(int c,int d);
		
	}
 abstract class child extends without_void{
	 int m1(int a) {
		 System.out.println(a);

		 return a;
	 }
String m2(int a,String s) {
		 
	System.out.println(a+""+s);
		 return "pass";
 
 }
 }
 
 abstract class child1 extends child{
	 boolean m3(int c,int d) {
		 boolean b=true;
		 System.out.println(c+""+d);

		if(c==d) {
			 System.out.println(b);

		}else 
			 System.out.println("false");

			return b;
			
	 }
 }
 class child2 extends child1{
	 public static void main(String []args) {
	
		 without_void wv=new child2();
		 wv.m1(10);
		 wv.m2(20,"laxman");
		 wv.m3(20, 20);
		
		
		 
	 }
 } 
 //In abstract class static and instance blocks will work
 // here iam using constructor also
 abstract class stat_inst_blocks{
	 static {
		 System.out.println("static block");
	 }
	 {
		 System.out.println("instance block");
		 
	 }
	 stat_inst_blocks(){
		 System.out.println("0 Argument constructor");
	 }
 }
 class Tests extends stat_inst_blocks{
	 Tests(){
		 super();
		 System.out.println("child class constructor");
	 }
 public static void main(String []args) {
	 new Tests();
 }
 }